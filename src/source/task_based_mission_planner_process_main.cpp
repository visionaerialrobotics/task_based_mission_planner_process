#include "task_based_mission_planner_process.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, ros::this_node::getName());
  TaskBasedMissionPlanner planner;
  planner.setUp();
  ros::Rate r(10);
  while (ros::ok())
  {
    ros::spinOnce();
    planner.run();
    r.sleep();
  }
  return 0;
}
