/*!*********************************************************************************
 *  \file       task_based_mission_planner_process.h
 *  \brief      TaskBasedMissionPlannerProcess definition file.
 *  \details    This file contains the TaskBasedMissionPlannerProcess declaration. To obtain more information about
 *              it's definition consult the task_based_mission_planner_process.cpp file.
 *  \authors    Guillermo De Fermin
 *  \copyright  Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/
#ifndef TASK_BASED_MISSION_PLANNER
#define TASK_BASED_MISSION_PLANNER

#include "ros/ros.h"
#include "drone_process.h"
#include <boost/regex.hpp>
#include <vector>
#include "Definitions.h"
#include "ParameterHandler.h"
#include "VariableHandler.h"
#include "Interpreter.h"
#include "aerostack_msgs/BehaviorEvent.h"
#include "aerostack_msgs/RequestBehavior.h"
#include "droneMsgsROS/InhibitBehavior.h"
#include "aerostack_msgs/BehaviorCommand.h"
#include "droneMsgsROS/AddBelief.h"
#include "droneMsgsROS/RemoveBelief.h"
#include "droneMsgsROS/ConsultBelief.h"
#include "droneMsgsROS/InitiateBehaviors.h"
#include "droneMsgsROS/CompletedMission.h"

class TaskBasedMissionPlanner: public DroneProcess
{
  public:
  //Constructor & Destructor.
  TaskBasedMissionPlanner();
  ~TaskBasedMissionPlanner();

  //DroneProcess functions.
  void ownSetUp();
  void ownStart();
  void ownStop();
  void ownRun();

private:
  int drone_id;
  ros::NodeHandle n;

  Mission mission;

  bool file_loaded;
  bool mission_started;
  bool waiting_for_behavior;
  bool last_behavior_interrupted;

  std::vector<EndingStepType> ending_steps;

  std::string last_behavior;

  std::string behavior_event_topic;

  std::string activate_behavior_service;
  std::string cancel_behavior_service;
  std::string add_belief_service;
  std::string remove_belief_service;
  std::string consult_belief_service;
  std::string initiate_behaviors_service;
  std::string completed_mission_str;

  ros::Subscriber behavior_event_subscriber;


  ros::ServiceClient activate_behavior_client;
  ros::ServiceClient cancel_behavior_client;
  ros::ServiceClient add_belief_client;
  ros::ServiceClient remove_belief_client;
  ros::ServiceClient consult_belief_client;
  ros::ServiceClient initiate_behaviors_client;
  ros::Publisher completed_mission_pub;

  //Topic Callbacks.
  void behaviorEventCallback(const aerostack_msgs::BehaviorEvent &msg);

  void nextCommand();

  void abortMission();
  void endMission();
  void abortLoadingMission();

  bool executeBehavior(Behavior behavior);
  bool inhibitBehavior(double behavior_uid);
  bool addBelief(std::string belief, bool multivalued);
  bool removeBelief(std::string belief);
  bool trueBelief(std::string belief);
  std::map<std::string, std::string> consultBelief(std::string belief);

  std::string substituteVariables(std::string s);

  void checkEvents();
};

#endif
